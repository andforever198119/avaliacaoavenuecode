#encoding: utf-8

Feature: Create Task/subtask

  As a ToDo App user
  I should be able to create tasks and subtasks
  So I can manage my tasks and subtasks

@teste1
Scenario: Successfully Add Task with add button
Given that I go to the site avenue code
When I access the link My Tasks
And fill in the task field
And click the add button
Then the task is added to the task list

@teste2
Scenario: Successfully Add Task with enter
Given that I access the application avenue code
When I access the functionality My Tasks
And fill in the task field below
And hit the enter button
Then the task is insert to the task list below

@teste3
Scenario: Add named task containing three characters
Given that I access the system avenue code
When I access My Tasks
And fill in the task field below with three characters
And click the add button on the right side of the screen
Then the task added successfully

@teste4
Scenario: Validate if the task appears in the added item list
Given that I enter in site avenue code
When I select My Tasks
And fill in the task field in system
And click the add button in system
Then appended on the list of created tasks

@teste5
Scenario: Validate If the Manage Tasks Button Is Displayed
Given I have the page avenue code open
When I'm within functionality My Tasks
And fill in the task field located on the page
And access the task add button
Then I see the subtask manager button

@teste6
Scenario: Validate if the number of included tasks is displayed in the manage subtasks button description
Given that I enter the system avenue code
When I open My tasks
And fill the task field
And access the plus button
And access my subtasks in Manage Subtasks
And include a subtask
Then verify label in button manage subtasks