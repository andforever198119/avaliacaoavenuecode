class Comum < SitePrism::Page
	
	def taskvalidate

		@waittime
		taskvalidate = page.find(:xpath, "/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr/td[2]/a")		
		converttaskvalidate = taskvalidate.text
		converttaskvalidatesemespacos=converttaskvalidate.gsub(/\s+/,"")
		if converttaskvalidatesemespacos.eql? "testar" then puts nil else raise "task not inserted" end		
	end

	def taskvalidatethreechar

		@waittime
		taskvalidate = page.find(:xpath, "/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr/td[2]/a")		
		converttaskvalidate = taskvalidate.text
		converttaskvalidatesemespacos=converttaskvalidate.gsub(/\s+/,"")
		if converttaskvalidatesemespacos.eql? "tes" then puts nil else raise "task not inserted" end		
	end

	def tasklist

		@waittime
		taskvalidate = page.find(:xpath, "/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr/td[2]/a")		
		converttaskvalidate = taskvalidate.text
		converttaskvalidatesemespacos=converttaskvalidate.gsub(/\s+/,"")
		if converttaskvalidatesemespacos.eql? "tes" then puts nil else raise "task not inserted" end
		@waittime
		taskvalidatelist = page.find(:xpath, "/html/body/div[1]/div[2]/div[2]/div/table")
		taskvalidatelistconvert = taskvalidatelist.text
		taskvalidatelistfound = taskvalidatelistconvert.scan(/tes/).count
		if taskvalidatelistfound == 1 then puts nil else raise "Existe mais de um item na lista" end		
	end

	def verifyelement

		page.find(:xpath, "/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr/td[4]/button").visible?
	end

	def verifycountsubtask

		validatesubtaskcount = page.find(:xpath, "/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr/td[4]/button")
		validatesubtaskcountconvert = validatesubtaskcount.text
		convertsubtasksemespacos=validatesubtaskcountconvert.gsub(/\s+/,"")
		if convertsubtasksemespacos.eql? "(1)ManageSubtasks" then puts nil else raise "subtask not validated" end
	end

	def wait

		aguardarelemento = page.find(:xpath,"/html/body/div[1]/div[1]/div/div[1]/a")
		until (aguardarelemento) 
		sleep 7
		end
	end

	def clear

		page.find(:xpath, "/html/body/div[1]/div[1]/div/div[2]/ul[2]/li[2]/a").click
		@task = Task.new
		@task.sign_page
		@waittimeshort
		page.find(:xpath, "/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[1]/td[5]/button").click
		@waittimeshort
		page.find(:xpath, "/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[1]/td[5]/button").click
		@waittimeshort
		page.find(:xpath, "/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[1]/td[5]/button").click
		@waittimeshort
		page.find(:xpath, "/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[1]/td[5]/button").click
		@waittimeshort
		page.find(:xpath, "/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[1]/td[5]/button").click
		@waittimeshort
		page.find(:xpath, "/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[1]/td[5]/button").click
		@waittimeshort
	end

	def waittime

		sleep 5
	end

	def waittimealong

		sleep 10
	end

	def waittimeshort

		sleep 3
	end
end