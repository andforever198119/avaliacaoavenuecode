class Task < SitePrism::Page	
	
	def sign_page

		$waitload=Comum.new
		$waitload.wait
		page.find(:xpath, "//*[@id='sign_in']").click
		find(:id, "user_email").send_keys("andforever.aa@gmail.com")
		find(:id, "user_password").send_keys("x42Na1Fu")
		page.find(:xpath, '//*[@id="new_user"]/input').click
		$waitload.wait
		page.find(:id, 'my_task').click
		$waitload.wait
	end

	def taskfield		

		find(:id, "new_task").send_keys("testar")			
	end

	def access
		
		page.find(:xpath, "/html/body/div[1]/div[2]/div[1]/form/div[2]/span").click
		$waitload.wait			
	end

	def taskfieldthreechar		

		find(:id, "new_task").send_keys("tes")			
	end

	def subtaskmanage

		page.find(:xpath, "/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr/td[4]/button").click
		@comum=Comum.new
		@comum.waittime
	end

	def createsubtask

		find(:xpath, "//*[@id='new_sub_task']").send_keys("subtaskteste")
		find(:xpath, "//*[@id='dueDate']").send_keys("10/01/2019")
		page.find(:id, 'add-subtask').click
		@comum.waittime
		page.find(:xpath, "/html/body/div[4]/div/div/div[3]/button").click		
	end
end