
	Given (/^that I access the system avenue code$/) do

		@home=Home.new
		@home.acessar
	end

	When (/^I access My Tasks$/) do

		@task = Task.new
		@task.sign_page
	end

	And (/^fill in the task field below with three characters$/) do

		@task.taskfieldthreechar
	end

	And (/^click the add button on the right side of the screen$/) do

		@task.access
	end

	Then (/^the task added successfully$/) do
		
		@comum=Comum.new
		@comum.taskvalidatethreechar
	end