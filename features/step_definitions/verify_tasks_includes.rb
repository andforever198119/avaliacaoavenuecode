
	Given (/^that I enter the system referring to the avenue code site$/) do

		@home=Home.new
		@home.acessar
	end

	When (/^I access My tasks in system$/) do

		@task = Task.new
		@task.sign_page
	end

	And (/^enter with data in the task field$/) do

		@task.taskfieldthreechar
	end

	And (/^select the plus button$/) do

		@task.access
	end

	And (/^click the button Manage Subtasks$/) do
		
		@comum=Comum.new
		@comum.verifyelement
	end

	And (/^add a Subtask$/) do
		
		@comum=Comum.new
		@comum.verifyelement
	end

	Then (/^the manage subtask button displays the number one indicating a subtask for that task$/) do
		
		@comum=Comum.new
		@comum.verifyelement
	end