
	Given (/^that I enter the system avenue code$/) do

		@home=Home.new
		@home.acessar
	end

	When (/^I open My tasks$/) do

		@task = Task.new
		@task.sign_page
	end

	And (/^fill the task field$/) do

		@task.taskfieldthreechar
	end

	And (/^access the plus button$/) do

		@task.access
	end

	And (/^access my subtasks in Manage Subtasks$/) do
		
		@task.subtaskmanage		
	end

	And (/^include a subtask$/) do

		@task.createsubtask
	end	

	Then (/^verify label in button manage subtasks$/) do
		
		@comum=Comum.new
		@comum.verifycountsubtask
		@comum.clear		
	end