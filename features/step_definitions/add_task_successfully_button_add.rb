
	Given (/^that I go to the site avenue code$/) do

		@home=Home.new
		@home.acessar
	end

	When (/^I access the link My Tasks$/) do

		@task = Task.new
		@task.sign_page
	end

	And (/^fill in the task field$/) do

		@task.taskfield
	end

	And (/^click the add button$/) do

		@task.access
	end

	Then (/^the task is added to the task list$/) do
		
		@comum=Comum.new
		@comum.taskvalidate
	end