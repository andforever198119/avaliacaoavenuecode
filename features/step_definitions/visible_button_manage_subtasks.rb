
	Given (/^I have the page avenue code open$/) do

		@home=Home.new
		@home.acessar
	end

	When (/^I'm within functionality My Tasks$/) do

		@task = Task.new
		@task.sign_page
	end

	And (/^fill in the task field located on the page$/) do

		@task.taskfieldthreechar
	end

	And (/^access the task add button$/) do

		@task.access
	end

	Then (/^I see the subtask manager button$/) do
		
		@comum=Comum.new
		@comum.verifyelement
	end