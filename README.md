#avaliacao
Setup automação de testes - Ruby + Capybara + Rails + BDD

Possuir a versão do ruby 2.3.3 instalada (https://rubyinstaller.org/downloads/)
Instalar a versão na raiz do seu diretorio
Instalar o terminal commander
Verificar se o ruby foi instalado -> ruby -v
Instalar Devkit (http://dl.bintray.com/oneclick/rubyinstaller/DevKit-mingw64-64-4.7.2-20130224-1432-sfx.exe)
Descompactar no diretório do ruby
Acessar cd C:\Ruby24-x64\devkit
Rodar ruby dk.rb init
Abra o arquivo config.yml 10.A ultima linha do arquivo deve conter o valor : -C:/Ruby24-x64 - Caso contrário faça o ajuste no arquivo. Não esqueça do caractere — antes do C:. 13.Entrar com comando ruby dk.rb install
Configuração do projeto

Clonar o projeto em git clone https://andforever198119@bitbucket.org/andforever198119/avaliacaoavenuecode.git
Rodar no terminal gem install bundler
Instalar o sublime Text 3
Abrir o projeto no sublime
Navegar até a pasta avaliacaoavenuecode pelo terminal
executar Bundle install ou colocar todas as gems que estão em gem file (gem install nome da gem)
Rodar cucumber para rodar todos os testes
Caso queira rodar testes individuais cucumber -t @complemento_localizado_na_feature
Observações:

Os cenários foram feitos de acordo com o solicitado
Para que os testes funcionem as configurações devem estar instaladas corretamente
--Fim

